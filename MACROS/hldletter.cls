\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{hldletter}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{letter}}
\ProcessOptions
\LoadClass[12pt,letterpaper]{letter}

\usepackage{graphicx}
\newsavebox{\firmhead}
\newsavebox{\firmfoot}

\newlength{\leftfield}
\newlength{\rightfield}
\setlength{\leftfield}{110mm}
\setlength{\rightfield}{43mm}

\setlength\textwidth{14cm}
\setlength\topmargin{0.5in}
\setlength\oddsidemargin{0.5in}
\setlength\footskip{1.25truein}
\sbox{\firmhead}
        {\parbox{\textwidth}
	{\begin{center}
	  \includegraphics[height=2.0cm]{oulogo}\\[14pt]
          {\em \LARGE The University of Oklahoma}\\[6pt]
          {\sffamily \footnotesize HOMER L. DODGE DEPARTMENT OF PHYSICS AND ASTRONOMY}\\
        \end{center}}}

\sbox{\firmfoot}
        {\parbox{\textwidth}
        {\begin{center}
        {\sffamily \scriptsize 440 West Brooks, Room 100, Norman, Oklahoma 73019-2061 USA
          ~+1 405-325-3961 }
        \end{center}}}

\renewcommand{\ps@firstpage}
   {\setlength{\headheight}{46pt}\setlength{\headsep}{38pt}%
    \renewcommand{\@oddhead}{\usebox\firmhead}%

\renewcommand{\@oddfoot}{\raisebox{-20pt}[0pt]{\usebox{\firmfoot}}}%

\renewcommand{\@evenhead}{}\renewcommand{\@evenfoot}{}}


\providecommand{\@evenhead}{}\providecommand{\@oddhead}{}
\providecommand{\@evenfoot}{}\providecommand{\@oddfoot}{}

\pagestyle{empty}

\renewcommand{\opening}[1]{\thispagestyle{firstpage}%
   \parbox[t]{\leftfield}
     {\parbox[b][4.5cm][c]{\leftfield}{\toname\\\toaddress}%
   \parbox[t]{\rightfield}
     {\@date}}
\par
\vspace{2\parskip} #1 \par\nobreak}




